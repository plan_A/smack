//
//  Channel.swift
//  Smack
//
//  Created by Jenia on 6/18/18.
//  Copyright © 2018 Jenia. All rights reserved.
//

import Foundation

struct Channel : Decodable {
    public private(set) var channelTitle: String!
    public private(set) var channelDescription: String!
    public private(set) var id: String!
}

